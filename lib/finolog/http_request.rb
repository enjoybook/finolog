require "faraday"
module Finolog
  module HTTPRequest
    extend self
    # def get(url, params: {}, headers: {})
    #   perform_request(url, method: :get, params: params, headers: headers)
    # end

    def post(url, body: {}, headers: {})
      perform_request(url, method: :post, body: body, headers: headers)
    end

    private

    def perform_request(url, method:, body:, headers:)
      conn = Faraday.new(url: url) do |faraday|
        faraday.request  :url_encoded

        faraday.response :logger do | logger |
          logger.filter(/(Api-token:)\s*\"(.+)\"/,'\1[REMOVED]')
          logger.filter(/(password=)(\w+)/,'\1[REMOVED]')
          # add here any sensetive credentials to replace them in logs
        end

        faraday.adapter  Faraday.default_adapter
      end

      if method == :post
        response = conn.post do |req|
          req.headers = headers
          req.body = body
          req.options.timeout = 30
          req.options.open_timeout = 10
        end

        raise Exception::RequestFailed.new(response.status, response.body) unless response.status == 200
        HTTPResponse.new(response)
      end
    end
  end
end
