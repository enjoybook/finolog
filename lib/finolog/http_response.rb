module Finolog
  class HTTPResponse
    attr_reader :code, :body

    def initialize(faraday_response)
      @code = faraday_response.status
      @body = faraday_response.body
    end
  end
end
