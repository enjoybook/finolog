module Finolog::Exception
  class RequestFailed < Exception
    attr_reader :code, :body
    def initialize(http_code, http_body)
      @code = http_code
      @body = http_body
      super("HTTP Request failed. Response code is #{@code}; body is #{@body}")
    end
  end
end
