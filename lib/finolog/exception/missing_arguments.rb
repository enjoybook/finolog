module Finolog::Exception
  class MissingArguments < Exception
    def initialize(missing_args)
      super("Can't find required arguments #{missing_args}")
    end
  end
end
