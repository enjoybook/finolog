module Finolog::Exception
  class IncorrectArgumentType < Exception
    def initialize(got, expected)
      super("Incorrect argument type. Expected: #{expected}, got: #{got}")
    end
  end
end
