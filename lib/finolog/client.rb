module Finolog
  class Client
    attr_reader :headers

    def initialize(token, headers: {})
      @headers = {
        "Api-Token": token
      }

      @headers.merge(headers)
    end

    # biz_id	Biz	ID бизнеса
    # company_id	Company	ID компании
    # category_id	Category	ID статьи
    # contractor_id	Contractor	ID контрагента
    # requisite_id	Requisite	ID реквизита контрагента
    # date	DateTime	Дата (Y-m-d HH:mm:ss)
    # from_id	Account	ID счета с которого происходит операция (передать только этот параметр для расхода, вместе с to_id для перевода)
    # to_id	Account	ID счета на который происходит операция (передать только этот параметр для прихода, вместе с from_id для перевода)
    # value	Float	Сумма операции (для операции прихода или расхода)
    # from_value	Float	Сумма расходной операции (для перевода, в валюте счета)
    # to_value	Float	Сумма приходной операции (для перевода, в валюте счета)
    # description	String	Описание
    def create_transaction(args = {})
      required_arguments(args, [:biz_id])

      biz_id = args.extract!(:biz_id).values[0]
      url = "https://api.finolog.ru/v1/biz/#{biz_id}/transaction/"

      HTTPRequest.post(url, body: args, headers: @headers)
    end

    private

    # Метод для проверки обязательных аргументов
    def required_arguments(hash, keys)
      expected_keys = keys.dup

      raise Exception::IncorrectArgumentType.new(keys.type, Array) unless keys.is_a? Array
      raise Exception::IncorrectArgumentType.new(hash.type, Hash) unless hash.is_a? Hash

      got_keys = hash.keys.dup
      missing_keys = []

      while expected_keys.size > 0
        looking_key = expected_keys.shift

        unless got_keys.include? looking_key
          missing_keys << looking_key
        end
      end

      raise Exception::MissingArguments.new(missing_keys) if missing_keys.size >= 1
    end
  end
end
